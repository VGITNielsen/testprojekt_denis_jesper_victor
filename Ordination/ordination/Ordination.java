package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public abstract class Ordination {
	private LocalDate startDen;
	private LocalDate slutDen;
	private Laegemiddel middel;

	/**
	 * Opretter en ordination
	 * 
	 * @param start
	 * @param slut
	 * @param middel
	 */
	public Ordination(LocalDate start, LocalDate slut, Laegemiddel middel) {
		this.startDen = start;
		this.slutDen = slut;
		this.middel = middel;
	}

	public LocalDate getStartDen() {
		return startDen;
	}

	public LocalDate getSlutDen() {
		return slutDen;
	}

	/**
	 * Antal hele dage mellem startdato og slutdato. Begge dage inklusive.
	 * 
	 * @return antal dage ordinationen gælder for
	 */
	public int antalDage() {
		return (int) ChronoUnit.DAYS.between(startDen, slutDen) + 1;
	}

	@Override
	public String toString() {
		return startDen.toString();
	}

	public void setMiddel(Laegemiddel middel) {
		this.middel = middel;
	}

	public Laegemiddel getLaegemiddel() {
		return middel;
	}

	/**
	 * Returnerer den totale dosis der er givet i den periode ordinationen er gyldig
	 * 
	 * @return
	 */
	public abstract double samletDosis();

	/**
	 * Returnerer den gennemsnitlige dosis givet pr dag i den periode ordinationen
	 * er gyldig
	 * 
	 * @return
	 */
	public abstract double doegnDosis();

	/**
	 * Returnerer ordinationstypen som en String
	 * 
	 * @return
	 */
	public abstract String getType();
}
