package ordination;

import java.util.ArrayList;

public class Patient {
	private String cprnr;
	private String navn;
	private double vaegt;
	private ArrayList<Ordination> ordinationer = new ArrayList<>();

	public Patient(String cprnr, String navn, double vaegt) {
		this.cprnr = cprnr;
		this.navn = navn;
		this.vaegt = vaegt;
	}

	public String getCprnr() {
		return cprnr;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public double getVaegt() {
		return vaegt;
	}

	public void setVaegt(double vaegt) {
		this.vaegt = vaegt;
	}

	@Override
	public String toString() {
		return navn + "  " + cprnr;
	}

	/**
	 * 
	 * @return a list of ordinations
	 */
	public ArrayList<Ordination> getOrdinationer() {
		return new ArrayList<>(ordinationer);
	}

	/**
	 * Adds an ordination to the list, if it isnt already contained in it.
	 * 
	 * @param o
	 */
	public void addOrdination(Ordination o) {
		if (!ordinationer.contains(o)) {
			ordinationer.add(o);
		}
	}

}
