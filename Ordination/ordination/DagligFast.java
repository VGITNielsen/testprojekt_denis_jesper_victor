package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;

public class DagligFast extends Ordination {

	public DagligFast(LocalDate start, LocalDate slut, Laegemiddel middel) {
		super(start, slut, middel);
		// TODO Auto-generated constructor stub
	}

	private Dosis[] dosis = new Dosis[4];

	@Override
	public double samletDosis() {
		return antalDage() * doegnDosis();
	}

	@Override
	public double doegnDosis() {
		double totalDosis = 0;
		for (Dosis d : dosis)
			totalDosis += d.getAntal();
		return totalDosis;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "Daglig fast";
	}

	public void opretDosis(LocalTime tid, double morgenAntal, double middagsAntal, double aftenAntal, double natAntal) {
		// TODO
		if (morgenAntal < 0 || middagsAntal < 0 || aftenAntal < 0 || natAntal < 0) {
			throw new IllegalArgumentException("Dosis kan ikke være negativ!");
		}
		if (morgenAntal == 0 & middagsAntal == 0 & aftenAntal == 0 & natAntal == 0) {
			throw new IllegalArgumentException("Total dosis kan ikke være 0 (nul)!");
		}
		Dosis dosis1 = new Dosis(tid, morgenAntal);
		Dosis dosis2 = new Dosis(tid, middagsAntal);
		Dosis dosis3 = new Dosis(tid, aftenAntal);
		Dosis dosis4 = new Dosis(tid, natAntal);
		dosis[0] = dosis1;
		dosis[1] = dosis2;
		dosis[2] = dosis3;
		dosis[3] = dosis4;
	}

	public Dosis[] getDoser() {
		// TODO Auto-generated method stub
		return Arrays.copyOf(dosis, 4);
	}

}
