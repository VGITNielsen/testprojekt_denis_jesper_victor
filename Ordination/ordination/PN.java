package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

	private double antalEnheder;
	private int antalGangeGivet;
	private ArrayList<LocalDate> antal = new ArrayList<>();

	public PN(LocalDate start, LocalDate slut, Laegemiddel middel, double antal) {
		super(start, slut, middel);
		this.antalEnheder = antal;
		// TODO Auto-generated constructor stub
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if (givesDen.isBefore(getSlutDen().plusDays(1)) && givesDen.isAfter(getStartDen().minusDays(1))) {
			antal.add(givesDen);
			antalGangeGivet++;
			return true;
		}
		return false;
	}

	public double doegnDosis() {
		if (antal.size() == 1) {
			return antalEnheder;
		}
		return samletDosis() / dageTaget();
	}

	public double samletDosis() {
		return antal.size() * antalEnheder;
	}

	public int dageTaget() {
		return (int) ChronoUnit.DAYS.between(antal.get(0), antal.get(antal.size() - 1)) + 1;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		return antalGangeGivet;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "PN";
	}

	public ArrayList<LocalDate> getAntal() {
		return new ArrayList<>(antal);
	}

}
