package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	/**
	 * 
	 * pre: paramters cannot be null 
	 */
	public DagligSkaev(LocalDate start, LocalDate slut, Laegemiddel middel) {
		super(start, slut, middel);
		// TODO Auto-generated constructor stub
	}

	private ArrayList<Dosis> dosises = new ArrayList<>();

	public void opretDosis(LocalTime tid, double antal) {
		// TODO
		Dosis dosis = new Dosis(tid, antal);
		dosises.add(dosis);
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * antalDage();
	}

	@Override
	public double doegnDosis() {
		double total = 0;
		for (int i = 0; i < dosises.size(); i++) {
			total += dosises.get(i).getAntal();
		}
		return total;
	}

	@Override
	public String getType() {
		return "Daglig skæv";
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(dosises);
	}

}
