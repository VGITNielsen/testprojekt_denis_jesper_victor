package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import ordination.DagligFast;
import ordination.Laegemiddel;

public class DagligFastTest {
	Laegemiddel lm1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
	DagligFast test1 = new DagligFast(LocalDate.of(2020, 4, 1), LocalDate.of(2020, 5, 1), lm1);
	DagligFast test2 = new DagligFast(LocalDate.of(2020, 4, 1), LocalDate.of(2020, 4, 8), lm1); // 7 days
	DagligFast test3 = new DagligFast(LocalDate.of(2020, 4, 1), LocalDate.of(2020, 5, 1), lm1); // 31 days
	DagligFast test4 = new DagligFast(LocalDate.of(2020, 4, 1), LocalDate.of(2021, 4, 1), lm1); // 365 days--1 year
	
	@Test(expected = IllegalArgumentException.class)
	public void testSamletDosis() {
		test2.opretDosis(LocalTime.of(12, 00), 0, 0, 0, 0);
		test2.samletDosis();
		test2.opretDosis(LocalTime.of(12, 00), 1, 1, 1, 1);
		assertEquals(28, test2.samletDosis(), 0.01);
		test2.opretDosis(LocalTime.of(12, 00), 1, 2, 3, 1);
		assertEquals(49, test2.samletDosis(), 0.01);
		test3.opretDosis(LocalTime.of(12, 00), 1, 1, 1, 1);
		assertEquals(124, test2.samletDosis(), 0.01);
		test3.opretDosis(LocalTime.of(12, 00), 1, 2, 2, 1);
		assertEquals(186, test2.samletDosis(), 0.01);
		test4.opretDosis(LocalTime.of(12, 00), 1, 1, 1, 2);
		assertEquals(1825, test2.samletDosis(), 0.01);
		test4.opretDosis(LocalTime.of(12, 00), 1, 4, 2, 2);
		assertEquals(3285, test2.samletDosis(), 0.01);
	}

	@Test
	public void testDoegnDosis() {
		test1.opretDosis(LocalTime.of(12, 00), 5, 0, 0, 0);
		assertEquals(5, test1.doegnDosis(), 0.01);
		test1.opretDosis(LocalTime.of(12, 00), 5, 5, 0, 0);
		assertEquals(10, test1.doegnDosis(), 0.01);
		test1.opretDosis(LocalTime.of(12, 00), 5, 5, 5, 0);
		assertEquals(15, test1.doegnDosis(), 0.01);
		test1.opretDosis(LocalTime.of(12, 00), 5, 5, 5, 6);
		assertEquals(21, test1.doegnDosis(), 0.01);
	}

	@Test
	public void testDagligFast() {
		DagligFast test11 = new DagligFast(LocalDate.of(2020, 4, 1), LocalDate.of(2020, 5, 1), lm1);
		assertArrayEquals(test1.getDoser(), test11.getDoser());
		assertEquals(test1.getLaegemiddel(), test11.getLaegemiddel());
		assertEquals(test1.getSlutDen(), test11.getSlutDen());
		assertEquals(test1.getStartDen(), test11.getStartDen());
		assertEquals(test1.getType(), test11.getType());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretDosis() {
		test1.opretDosis(LocalTime.of(12, 00), 0, -1, 0, 0);
		test1.opretDosis(LocalTime.of(12, 00), 0, 0, 0, 0);
		test1.opretDosis(LocalTime.of(12, 00), 1, 2, 3, 4);
	}

}
