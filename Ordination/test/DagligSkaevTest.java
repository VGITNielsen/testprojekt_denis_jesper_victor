package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Test;

import controller.Controller;
import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;

public class DagligSkaevTest {
	Laegemiddel middel = new Laegemiddel("test", 10, 20, 30, "testeren");
	DagligSkaev enDag = new DagligSkaev(LocalDate.of(2020, 01, 01), LocalDate.of(2020, 01, 01), middel);
	DagligSkaev fireDage = new DagligSkaev(LocalDate.of(2020, 01, 01), LocalDate.of(2020, 01, 04), middel);
	
	
	
	
	@Test
	public void testSamletDosis() {
		enDag.opretDosis(LocalTime.of(10, 00), 5);
		fireDage.opretDosis(LocalTime.of(10, 00), 5);
		
		// 5 pills one day = 5 in total
		assertEquals(5, enDag.samletDosis(), 0);
		// 5 pills four days = 20 in total 
		assertEquals(20, fireDage.samletDosis(), 0);
	}

	@Test
	public void testDoegnDosis() {
		enDag.opretDosis(LocalTime.of(10, 00), 5);
		assertEquals(5, enDag.doegnDosis(), 0);
		enDag.opretDosis(LocalTime.of(11, 00), 6.5);
		assertEquals(11.5, enDag.doegnDosis(), 0);
		enDag.opretDosis(LocalTime.of(23, 00), 10);
		assertEquals(21.5, enDag.doegnDosis(), 0);
	}

	@Test
	public void testDagligSkaev() {
		DagligSkaev testObjekt = new DagligSkaev(LocalDate.of(2020, 01, 01), LocalDate.of(2020, 01, 01), middel);
		//tester testObjekt
		assertEquals(enDag.getStartDen(), testObjekt.getStartDen());
		assertEquals(enDag.getSlutDen(), testObjekt.getSlutDen());
		assertEquals(enDag.getLaegemiddel(), testObjekt.getLaegemiddel());
	}

	@Test
	public void testOpretDosis() {
		enDag.opretDosis(LocalTime.of(10, 30), 5);
		Dosis exp = new Dosis(LocalTime.of(10, 30), 5);
		assertEquals(exp.getAntal(), enDag.getDoser().get(0).getAntal(), 0);
		assertEquals(exp.getTid(), enDag.getDoser().get(0).getTid());
	}

}
