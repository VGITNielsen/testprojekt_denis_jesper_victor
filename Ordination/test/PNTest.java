package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;

public class PNTest {

	private Laegemiddel l = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
	private PN pn = new PN(LocalDate.of(2020, 3, 5), LocalDate.of(2020, 3, 9), l, 5);

	@Test
	public void testSamletDosis() {
		pn.givDosis(LocalDate.of(2020, 3, 6));
		pn.givDosis(LocalDate.of(2020, 3, 7));
		assertEquals(10, pn.samletDosis(), 0.0001);
	}

	@Test
	public void testDoegnDosis1() {
		pn.givDosis(LocalDate.of(2020, 3, 6));
		pn.givDosis(LocalDate.of(2020, 3, 7));
		assertEquals(5, pn.doegnDosis(), 0.0001);
	}

	@Test
	public void testDoegnDosis2() {
		pn.givDosis(LocalDate.of(2020, 3, 6));
		pn.givDosis(LocalDate.of(2020, 3, 8));
		assertEquals(3.33333, pn.doegnDosis(), 0.0001);
	}

	@Test
	public void testDoegnDosis3() {
		pn.givDosis(LocalDate.of(2020, 3, 6));
		pn.givDosis(LocalDate.of(2020, 3, 9));
		assertEquals(2.5, pn.doegnDosis(), 0.0001);
	}

	@Test
	public void testPN() {
		PN pn1 = new PN(LocalDate.of(2020, 3, 5), LocalDate.of(2020, 3, 9), l, 5);
		assertEquals(pn1.getStartDen(), pn.getStartDen());
		assertEquals(pn1.getSlutDen(), pn.getSlutDen());
		assertEquals(pn1.getLaegemiddel(), pn.getLaegemiddel());
		assertEquals(pn1.getAntalEnheder(), pn.getAntalEnheder(), 0.0001);
		assertEquals(pn1.getType(), pn.getType());
	}

	@Test
	public void testGivDosis() {
		assertTrue(pn.givDosis(LocalDate.of(2020, 3, 6)));
		assertTrue(pn.givDosis(LocalDate.of(2020, 3, 9)));
		assertTrue(pn.givDosis(LocalDate.of(2020, 3, 5)));
		assertFalse(pn.givDosis(LocalDate.of(2020, 3, 4)));
		assertFalse(pn.givDosis(LocalDate.of(2020, 3, 10)));

	}

}
