package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ControllerTest {

	private Laegemiddel l = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
	private Patient p = new Patient("123456-7890", "Karl karlsen", 90.5);

	@Test
	public void testOpretPNOrdination() {
		LocalDate start = LocalDate.of(2020, 3, 4);
		LocalDate slut = LocalDate.of(2020, 3, 9);
		PN pn = new PN(start, slut, l, 5);
		PN pn1 = Controller.getController().opretPNOrdination(start, slut, p, l, 5);
		assertEquals(pn1.getStartDen(), pn.getStartDen());
		assertEquals(pn1.getSlutDen(), pn.getSlutDen());
		assertEquals(pn1.getLaegemiddel(), pn.getLaegemiddel());
		assertEquals(pn1.getAntalEnheder(), pn.getAntalEnheder(), 0.0001);
		assertEquals(pn1.getType(), pn.getType());
	}

	@Test
	public void testOpretDagligFastOrdination() {
		LocalDate start = LocalDate.of(2020, 3, 4);
		LocalDate slut = LocalDate.of(2020, 3, 9);
		DagligFast df = Controller.getController().opretDagligFastOrdination(start, slut, p, l, 1, 2, 1, 3);
		DagligFast df1 = new DagligFast(start, slut, l);
		assertEquals(df.getStartDen(), df.getStartDen());
		assertEquals(df.getSlutDen(), df.getSlutDen());
		assertEquals(df.getLaegemiddel(), df.getLaegemiddel());
		assertEquals(df.getType(), df1.getType());

	}

	@Test
	public void testOpretDagligSkaevOrdination() {
		LocalDate start = LocalDate.of(2020, 3, 4);
		LocalDate slut = LocalDate.of(2020, 3, 9);
		LocalTime[] tider = { LocalTime.now(), LocalTime.now(), LocalTime.now() };
		double[] antal = { 1, 1, 1 };
		DagligSkaev ds = Controller.getController().opretDagligSkaevOrdination(start, slut, p, l, tider, antal);
		DagligSkaev ds1 = new DagligSkaev(start, slut, l);
		assertEquals(ds.getStartDen(), ds1.getStartDen());
		assertEquals(ds.getSlutDen(), ds1.getSlutDen());
		assertEquals(ds.getLaegemiddel(), ds1.getLaegemiddel());
	}

	@Test
	public void testOrdinationPNAnvendt() {
		LocalDate start = LocalDate.of(2020, 3, 4);
		LocalDate slut = LocalDate.of(2020, 3, 9);
		PN pn = new PN(start, slut, l, 5);
		pn.givDosis(LocalDate.of(2020, 3, 5));
		Controller.getTestController().ordinationPNAnvendt(pn, LocalDate.of(2020, 3, 5));
		assertEquals(1, pn.getAntal().size() - 1, 0.000000001);
	}

	@Test
	public void testAnbefaletDosisPrDoegn() {
		p.setVaegt(20.0);
		assertEquals(1, Controller.getTestController().anbefaletDosisPrDoegn(p, l), 0.001);
		p.setVaegt(25.5);
		assertEquals(1.5, Controller.getTestController().anbefaletDosisPrDoegn(p, l), 0.001);
		p.setVaegt(119.9);
		assertEquals(1.5, Controller.getTestController().anbefaletDosisPrDoegn(p, l), 0.001);
		p.setVaegt(120.5);
		assertEquals(2.0, Controller.getTestController().anbefaletDosisPrDoegn(p, l), 0.001);
	}

}
